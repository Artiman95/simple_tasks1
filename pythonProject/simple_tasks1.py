# Task 1

list1 = [14, 21, 31, 111]


def sum_num1(num):
    summ = 0
    while num:
        summ += num % 10
        num //= 10
    return summ


print(sorted(list1, key=sum_num1))


# Task 2

dict1 = {12: 24, 13: 15, 16: 88}
dict2 = {12: 34, 13: 15, 17: 89}

dictionary = dict1

for key, value in dict2.items():
    if key in dict1:
        dictionary[key] += value
    else:
        dict.update({key: value})

print(dictionary)


# Task 3

from collections import Counter

string = 'aa bb aa cc dd dd'
list2 = string.split(' ')
cnt = Counter(list2)
print(cnt)


# Task 4

list3 = ['aa13ara', 'aa123a', 'aar13a', 'aa13']


def length(string1):
    count = 0
    for i in string1:
        count += 1
    return count


print(sorted(list3, key=length))


# Task 5

list4 = [123321, 12311, 'dad', 987789, 'python', 'tenet', 47587]


for word in list4:
    if str(word) == str(word)[:: -1]:
        print(word)


# Task 6


class CarColor:
    def __init__(self, color):
        self.color = color

    def srv(self):
        if self.color == '':
            print('Цвет для машины ещё не выбран')
        else:
            print('Машина цвета ' + self.color)


a = CarColor('синий')
a.srv()
